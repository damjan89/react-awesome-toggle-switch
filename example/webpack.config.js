const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const htmlWebpackPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
});

module.exports = {
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
             test: /\.module\.s(a|c)ss$/,
       loader: [
       MiniCssExtractPlugin.loader,
           {
             loader: 'css-loader',
           options: {
               modules: true,
               sourceMap: true
             }
         },
         {
           loader: 'sass-loader',
           options: {
               sourceMap: true
             }
         }
       ]
     },
     {
       test: /\.s(a|c)ss$/,
       exclude: /\.module.(s(a|c)ss)$/,
       loader: [
            MiniCssExtractPlugin.loader,
           'css-loader',
           {
             loader: 'sass-loader',
           options: {
               sourceMap: true
             }
         }
       ]
     }
    ]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  plugins: [
    htmlWebpackPlugin,
    new MiniCssExtractPlugin({filename:  '[name].css', chunkFilename: '[id].css'})]
};
