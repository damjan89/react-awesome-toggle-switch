import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class Neon2Switch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){

    }
    onChange(e:any){
        this.setState({
            val: e.currentTarget.checked
        });
        if(e.currentTarget.checked){
            this.state.nodeRef.current.className='toggle toggle-on';
        } else {
            this.state.nodeRef.current.className='toggle';
        }
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <div className={"neon2Switch"}>
                <div className={'toggle ' + this.state.val ? 'toggle-on': ''} id='switch' ref={this.state.nodeRef} onClick={(e)=>this.onChange({currentTarget:{checked: !this.state.val}})}>
                    <div className='toggle-text-off'>{this.props.config.rightText}</div>
                    <div className='glow-comp'></div>
                    <div className='toggle-button'></div>
                    <div className='toggle-text-on'>{this.props.config.leftText}</div>
                </div>
            </div>
        )
    }
}
