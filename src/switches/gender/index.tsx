import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class GenderSwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
                <div className="genderSwitch">
                    <input id={"switch"} type="checkbox" hidden ref={this.state.nodeRef} onChange={(e)=>this.onChange(e)}/>
                        <label htmlFor="switch" className={"switch"}>
                            <h1 className={"female"}>✝</h1>
                            <h1 className={"male"}>➜</h1>
                        </label>

                </div>
        )
    }
}
