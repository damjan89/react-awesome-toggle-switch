import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class TotoroSwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <div className={"totoroSwitch"}>
                <div className='wrap'>
                    <input type='checkbox' ref={this.state.nodeRef} onChange={(e)=>this.onChange(e)}/>
                        <div className='totoro'>
                            <div className='ears'>
                                <div className='ear'></div>
                                <div className='ear'></div>
                            </div>
                            <div className='arm'></div>
                            <div className='arm'></div>
                            <div className='foot'></div>
                            <div className='foot two'></div>
                            <div className='body'>
                                <div className='spots'>
                                    <div className="spot"></div>
                                    <div className="spot"></div>
                                    <div className="spot"></div>
                                    <div className="spot"></div>
                                    <div className="spot"></div>
                                    <div className="spot"></div>
                                    <div className="spot"></div>
                                </div>
                                <div className='inner'>
                                    <div className='mouth'></div>
                                    <div className="eye"></div>
                                    <div className="eye"></div>
                                </div>
                            </div>
                        </div>
                </div>

            </div>
        )
    }
}
