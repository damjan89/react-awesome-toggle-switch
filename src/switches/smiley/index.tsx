import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class SmileySwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <div className={"smileySwitch"}>
                <div id="container">
                    <div className="toggle">
                        <input type="checkbox" name="toggle" className="check-checkbox" id="mytoggle" ref={this.state.nodeRef} onChange={(e)=>this.onChange(e)}/>
                            <label className="check-label" htmlFor="mytoggle">
                                <div id="background"></div>
                                <span className="face">
                                    <span className="face-container">
                                      <span className="eye left"></span>
                                      <span className="eye right"></span>
                                      <span className="mouth"></span>
                                    </span>
                                </span>
                            </label>
                    </div>
                </div>
            </div>
        )
    }
}
