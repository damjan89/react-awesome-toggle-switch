import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class LampSwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <div className={"lampSwitch"}>
                <div className={"mid"}>
                    <label className={"rocker rocker-small"}>
                        <input type={"checkbox"}  ref={this.state.nodeRef} onChange={(e)=>this.onChange(e)}/>
                            <span className={"switch-left"}>{this.props.config.leftText}</span>
                            <span className={"switch-right"}>{this.props.config.rightText}</span>
                    </label>
                </div>
            </div>
        )
    }
}
