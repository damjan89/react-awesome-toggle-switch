import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class FlatSwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <div className={"flatSwitch"}>
                <input id="checkbox" type="checkbox" ref={this.state.nodeRef} onChange={(e)=>this.onChange(e)}/>
                <label htmlFor="checkbox">
                    <div className="s">
                        <div className="d"></div>
                        <div className="d"></div>
                        <div className="d"></div>
                        <div className="d"></div>
                        <div className="d"></div>
                        <div className="d"></div>
                        <div className="d"></div>
                        <div className="d"></div>
                        <div className="d"></div>
                    </div>
                </label>
             </div>
        )
    }
}
