import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any
}
export interface IState {
    nodeRef: any,
    val: any,
}
export default class DefaultSwitch extends React.Component<IProps, IState>{
    public val:boolean;
    public nodeRef: any;
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <label className={"checker"}>
                <input ref={this.state.nodeRef} className={"checkbox"} type="checkbox" onChange={(e)=>this.onChange(e)}/>
                <div className={"check-bg"}></div>
                <div className={"checkmark"}>
                    <svg viewBox="0 0 100 100">
                        <path d="M20,55 L40,75 L77,27" fill="none" stroke="#FFF" strokeWidth="15" strokeLinecap="round" strokeLinejoin="round"/>
                    </svg>
                </div>
            </label>
        )
    }
}
