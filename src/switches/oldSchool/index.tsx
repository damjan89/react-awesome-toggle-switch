import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any

}
export interface IState {
    nodeRef: any,
    val: any
}
export default class OldSchoolSwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <div className={"oldSchoolSwitch"}>
                <main>
                    <div className={"item"}>
                        <h3>{this.props.config.title}</h3>
                        <div className={"knob-wrap"}>
                            <input ref={this.state.nodeRef} type={"checkbox"} className={"knob"}  onChange={(e)=>this.onChange(e)}/>
                            <span className={"light"}></span>
                        </div>
                    </div>
                </main>
            </div>
        )
    }
}
