import * as React from "react";
import '../../assets/style.scss'
export interface IProps {
    value: boolean,
    onValueChange: any,
    config: any,
    title?: string

}
export interface IState {
    nodeRef: any,
    val: any,
    title: string
}
export default class RadialSwitch extends React.Component<IProps, IState>{
    constructor(props:any){
        super(props);
        this.state = {
            nodeRef: React.createRef(),
            val: props.value ? props.value : false,
            title: props.title ? props.title : '',
        }
    }
    componentDidMount(){
        let nodeRef = this.state.nodeRef;
        nodeRef.current.checked = this.state.val;
        this.setState({nodeRef: nodeRef});
    }
    onChange(e:any){
        this.props.onValueChange(e.currentTarget.checked);
    }
    render(){
        return (
            <div className={"radialSwitch"}>
                <div className={"container"}>
                    <h3>{this.props.config && this.props.config.title ? this.props.config.title : ''}</h3>
                    <div className={"checkbox-container yellow"}>
                        <input ref={this.state.nodeRef} type={"checkbox"} id="toggle1" onChange={(e)=>this.onChange(e)}/>
                        <label htmlFor={"toggle1"}></label>
                        <div className={"active-circle"}></div>
                    </div>
                </div>
            </div>
        )
    }
}
