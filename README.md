# react-awesome-toggle-switch

> ReactJS 19 different toggle switches designs in one component

[![NPM](https://img.shields.io/npm/v/react-money.svg)](https://www.npmjs.com/package/react-awesome-toggle-switch) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm i react-awesome-toggle-switch --save
```
## GitLab
https://gitlab.com/damjan89/react-awesome-toggle-switch
## Usage
React >= 16.9.0
```tsx
import AwesomeToggleSwitch from "react-awesome-toggle-switch";
export default class ReactAwesomeToggleSwitchExample extends React.Component<{}, IState> {
  constructor(props:any) {
      super(props);
      this.state = {
          val: true,
          config:{
            toggleTheme: 'DefaultSwitch',
            title: 'Title',
            leftText:'On',
            rightText:'Off',
          }
      }
  }
  valChanged(e:any){
      this.setState({
          val: e
      });
  }
  render() {
    return (
    <div style={{width: '100%'}}>
        <AwesomeToggleSwitch value={this.state.val} onChange={(e:any)=>this.valChanged(e)} config={this.state.config}></AwesomeToggleSwitch>
    </div>
    );
  }
}
ReactDOM.render(<ReactAwesomeToggleSwitchExample/>, document.getElementById('root'));
```

### Toggle Themes (this.state.config.toggleTheme)
```
DefaultSwitch
RadialSwitch
SimpleSwitch
OldSchoolSwitch
GenderSwitch
BulbSwitch
LampSwitch
LockSwitch
NeonSwitch
ZedDashSwitch
TotoroSwitch
ElasticSwitch
SmileySwitch
RollingSwitch
DayNightSwitch
InvertSwitch
FlatSwitch
Neon2Switch
BearSwitch
```

## Preview
![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/default.png) -> DefaultSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/bear.png) -> BearSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/bulb.png) -> BulbSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/dayNight.png) -> DayNightSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/elastic.png) -> ElasticSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/flat.png) -> FlatSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/gender.png) -> GenderSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/invert.png) -> InvertSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/lamp.png) -> LampSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/lock.png) -> LockSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/neon.png) -> NeonSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/neon2.png) -> Neon2Switch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/oldSchool.png) -> oldSchoolSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/radial.png) -> RadialSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/rolling.png) -> RollingSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/simple.png) -> SimpleSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/smiley.png) -> SmileySwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/totoro.png) -> TotoroSwitch

![https://freefrontend.com/css-toggle-switches/](https://gitlab.com/damjan89/react-awesome-toggle-switch/raw/master/zedDash.png) -> ZenDashSwitch


## License
Special Thanks to Vladimir Stepura for this post (all pure html & css can be found in url bellow) and all other developers/designers 
url: https://freefrontend.com/css-toggle-switches/
MIT © [Nick Dam](https://gitlab.com/damjan89)
